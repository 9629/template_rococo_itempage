#３社サブタイトル変更

各サイトそれぞれにキャッチコピー、商品情報の箇所でサブタイトル、キャッチを入れる

##楽天

###キャッチコピー
<font color="#660000" size="-1"><b>【 シャワーも海も 水 OK! / メール便対応 / 15時まで発送 / 土日祝発送 】</b></font><br>
ワックスコードアンクレット <br>

###モバイル用キャッチコピー
    シャワーも海も水OK! メール便対応 15時まで発送/土日祝発送　
yahoo(キャッチ)と一緒 末に全角スペース


##yahoo

###キャッチコピーと商品情報に入れる

####* キャッチ(30文字) 送料無料入れない
    シャワーも海も水OK! メール便対応 午後3時まで即日配送/土日祝発送　

####* 商品情報
    * 【 シャワーも海も 水 OK! / メール便対応 / 15時まで発送 / 土日祝発送 】

###一言コメント(html可)全角500文字（1000バイト）以内
<div id="hitokoto">
<ul>

<li>
<div id="comment">
<p>ベタベタせず水に強いワックスコードで編みこんだスタイリッシュな2トーンアンクレット。</p>
<div id="gotoDetail">
<a href="#CentItemCaption1"><img src="http://lib.shopping.srv.yimg.jp/lib/rococo/cart_goto_detail.png" alt="商品へ"></a>
</div></div>
</li>

<li>
<a href="#nico"><img src="http://lib.shopping.srv.yimg.jp/lib/rococo/cart_nico_free.png" alt="アクセ2個set無料"></a>
</li>


<li>
<a href="http://store.shopping.yahoo.co.jp/rococo/guide.html#mailbin">
<img src="http://lib.shopping.srv.yimg.jp/lib/rococo/cart_mailbin.png" alt="メール便"></a>
</li>

</ul>
</div>

--- yahoo ---
商品説明始まり箇所
id="CentItemCaption1"

border:1px solid #fbb7b7;

##makeShop

###商品別特殊表示

    【 シャワーも海も 水 OK! / メール便対応 / 午後3時まで即日配送 / 土日祝発送 】

-----
###Memo

####URL
**makeShop** 
http://www.rococoweb.com/rococo/

**Triple**
http://shopping.geocities.jp/rococo/



#####PC版で見る rakuten
**テーブル版**

    <table bgcolor="#b00000" cellspacing="15" cellpadding="2" width="100%">
    <tr>
    <th>

    <p align="center">
    <a href="http://item.rakuten.co.jp/rococostore/9999999/?force-site=pc&l2-id=shop_sp_pcview_icon">
    <font color="#fff" size="3">
    <b>詳しい内容をPC版で見る<font size="4"> &gt;&gt;</font></b>
    </font></a>
    </p>

    </th></tr>
    </table>
    <br><br>

**バナー版**

    <p align="center">
    <a href="http://item.rakuten.co.jp/rococostore/999999/?force-site=pc&l2-id=shop_sp_pcview_icon"><img src="http://www.rakuten.ne.jp/gold/rococostore/images/sp/to_pc-bnr.jpg" alt="PCへ" width="100%"></a>
    </p>
    <br />

#####ＰＣ版を見る makeShop
    <table bgcolor="#b00000" cellspacing="15" cellpadding="2" width="100%">
    <tr>
    <th>

    <p align="center">
    <a href="/shopdetail/99999999/pc_detail/">
    <font color="#fff" size="3">
    <b>詳しい内容をPC版で見る<font size="4"> &gt;&gt;</font></b>
    </font></a>
    </p>

    </th></tr>
    </table>
    <br><br>

#####PC版で見る yahooのみバナー画象
    <p align="center">
        <a href="http://store.shopping.yahoo.co.jp/rococo/999999.html?mode=pc"><img src="http://shopping.c.yimg.jp/lib/rococo/to_pc-bnr.jpg" alt="PCへ" width="100%"></a>
    </p>
    <br />

#####TOPへ戻るバナー　yahooのみ
    <br>
    <a href="http://shopping.geocities.jp/rococo/sp/index.html"><img src="http://shopping.c.yimg.jp/lib/rococo/sp_top.jpg" width="100%"></a>
    <br>

#####レビュースマホ版
    <br>
    <p>
        <a href="http://www.rakuten.ne.jp/gold/rococostore/images/review_present/sp_review_misanga.jpg"><img src="http://www.rakuten.ne.jp/gold/rococostore/images/review_present/sp_review_misanga.jpg" width="100%" alt=""/></a>
    </p>

####スマホ用バナーimg
    sp_review_wildthings-bandana.jpg
    sp_review_waxbless.jpg
    sp_review_waxanklet.jpg
    sp_review_shushan.jpg
    sp_review_polish.jpg
    sp_review_misanga.jpg
    sp_review_mailfree.jpg
    sp_review_leatherbless.jpg
    sp_review_keyholder.jpg
    sp_review_Karabiner.jpg
    sp_review_fidelity-bandana.jpg
    sp_review_elephant-bandana.jpg
    sp_review_delifree.jpg
    sp_review_blessleather.jpg
    sp_review_bandana.jpg
    sp_review_nau_cup.jpg

#####makeShop メール便対応
    <table width="100%" border="0" cellpadding="10" cellspacing="0">
    <tr>
    <th bgcolor="#E5E5E5"><b>■こちらの商品はメール便で発送が可能です</b></th>
    </tr>
    <tr>
    <td bgcolor="#E5E5E5">
    <font color="red">※メール便送料は全国一律200円です。<br />※「代金引換」希望の場合はご利用いただけません。</font>
    <a href="http://www.rococoweb.com/rococo/infomation.html#deliveryMail2">詳しくはコチラから</a></td>
    </tr>
    </table>

######yahooサイズ詳細バナー
    <br>
    <a href="http://shopping.geocities.jp/rococo/sp/sp_size.html"><img src="http://shopping.c.yimg.jp/lib/rococo/sp_size_link.gif" width="100%"></a>
    <br>

######yahoo丈直しバナー
    <br>
    <a href="http://shopping.geocities.jp/rococo/sp/sp_take.html"><img src="http://shopping.c.yimg.jp/lib/rococo/sp_take_link.gif" width="100%"></a>
    <br>
    <br>
